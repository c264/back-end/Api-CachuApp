# Api-CachuApp

## Sobre
- Api para Requisições para o guia de Cachoeiras feito em SpringBoot, Java, JPA 

## Tecnologias utilizadas
- Spring-Boot
- Linguagem Java
- Lombok
- JPA

## Requisitos
- PostgreSql
- Heroku

## Executando o projeto
### Clonando o projeto
```bash
$ https://github.com/clebernascimento/Api-CachuApp.git
```
