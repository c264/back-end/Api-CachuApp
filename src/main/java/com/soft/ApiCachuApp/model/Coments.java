package com.soft.ApiCachuApp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "tb_coments")
@EntityListeners(AuditingEntityListener.class)
public class Coments implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "increment", strategy = "increment")
    @Id
    @Column(name = "com_id")
    private Long id;

    @JsonIgnore
    @JoinColumn(name = "com_wat_id", referencedColumnName = "wat_id")
    @ManyToOne
    private Waterfall waterfall;

    @Column(name = "com_text")
    private String text;
}
