package com.soft.ApiCachuApp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Entity
@Table(name = "tb_waterfall")
@EntityListeners(AuditingEntityListener.class)
public class Waterfall implements Serializable{

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "increment", strategy = "increment")
    @Id
    @Column(name = "wat_id")
    private Long id;

    @Column(name = "wat_image")
    private String image;

    @Column(name = "wat_name")
    private String name;

    @Column(name = "wat_link")
    private String link;

    @Column(name = "wat_longitude")
    private String longitude;

    @Column(name = "wat_latitude")
    private String latitude;

    @Column(name = "wat_localization")
    private String localization;

    @Column(name = "wat_datetime")
    private String datetime;

    @Column(name = "wat_description")
    private String description;
}
