package com.soft.ApiCachuApp.controller;


import com.soft.ApiCachuApp.dto.WaterfallDTO;
import com.soft.ApiCachuApp.model.Waterfall;
import com.soft.ApiCachuApp.repository.WaterfallRepository;
import com.soft.ApiCachuApp.validation.ObjectNotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/waterfall")
public class WaterfallController {

    @Autowired
    private WaterfallRepository waterfallRepository;

    @ApiOperation("Register waterfall one at a time")
    @PostMapping("/register/waterfall")
    public ResponseEntity<Waterfall> save(@RequestBody WaterfallDTO dto) {
        Waterfall waterfall1 = new Waterfall();

        persist(dto, waterfall1);

        return ResponseEntity.ok().body(waterfall1);
    }

    @ApiOperation(" waterfall query, returning all in one list")
    @GetMapping
    public ResponseEntity<List<Waterfall>> getAll() {
        List<Waterfall> waterfallList = new ArrayList<>();
        waterfallList = waterfallRepository.findAll();
        return new ResponseEntity<>(waterfallList, HttpStatus.OK);
    }

    @ApiOperation("waterfall query, one at a time")
    @RequestMapping(path = "/{id}")
    public ResponseEntity<Optional<Waterfall>> getById(@PathVariable Long id) {
        Optional<Waterfall> waterfallId;
        try {
            waterfallId = waterfallRepository.findById(id);
            return ResponseEntity.ok().body(waterfallId);
        } catch (NoSuchElementException exception) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation("Deletes waterfall, one at a time")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Optional<Waterfall>> deleteById(@PathVariable Long id) {
        try {
            waterfallRepository.deleteById(id);
            return new ResponseEntity<Optional<Waterfall>>(HttpStatus.OK);
        } catch (NoSuchElementException exception) {
            return new ResponseEntity<Optional<Waterfall>>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation("Update waterfall, one at a time")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Waterfall> update(@PathVariable Long id, @RequestBody WaterfallDTO dto) {
        Waterfall waterfall = waterfallRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Registro não encontrado: " + id));
        persist(dto, waterfall);
        return ResponseEntity.ok().body(waterfall);
    }

    private void persist(WaterfallDTO dto, Waterfall waterfall1) {
        waterfall1.setName(dto.getName());
        waterfall1.setImage(dto.getImage());
        waterfall1.setLink(dto.getLink());
        waterfall1.setLongitude(dto.getLongitude());
        waterfall1.setLatitude(dto.getLatitude());
        waterfall1.setLocalization(dto.getLocalization());
        waterfall1.setDatetime(dto.getDatetime());
        waterfall1.setDescription(dto.getDescription());

        waterfallRepository.save(waterfall1);
    }
}