package com.soft.ApiCachuApp.controller;

import com.soft.ApiCachuApp.dto.ComentsDTO;
import com.soft.ApiCachuApp.model.Coments;
import com.soft.ApiCachuApp.model.Waterfall;
import com.soft.ApiCachuApp.repository.ComentsRepository;
import com.soft.ApiCachuApp.repository.WaterfallRepository;
import com.soft.ApiCachuApp.validation.ObjectNotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/coments")
public class ComentsController {

    @Autowired
    private ComentsRepository comentsRepository;

    @Autowired
    private WaterfallRepository waterfallRepository;

    @ApiOperation("Register coments one at a time")
    @PostMapping("/register")
    public ResponseEntity<Coments> save(@RequestBody ComentsDTO dto) {
        Coments coments = new Coments();

        persist(dto, coments);

        return ResponseEntity.ok().body(coments);
    }

    @ApiOperation("coments, query, returning all in on list")
    @GetMapping()
    public ResponseEntity<List<Coments>> getAll() {
        List<Coments> comentsList = new ArrayList<>();
        comentsList = comentsRepository.findAll();
        return new ResponseEntity<>(comentsList, HttpStatus.OK);
    }

    @ApiOperation("coments query one at a time")
    @RequestMapping(path = "/waterfall/{id}")
    public ResponseEntity<List<Coments>> getById(@PathVariable Long id){
        List<Coments> comentsId;
        try {
            comentsId = comentsRepository.findByWaterfallId(id);
            return ResponseEntity.ok().body(comentsId);
        }catch (NoSuchElementException exception){
            return ResponseEntity.notFound().build();
        }
    }

    @ApiOperation("Deletes coments, one at a time")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Optional<Coments>> deleteById(@PathVariable Long id) {
        try {
            comentsRepository.deleteById(id);
            return new ResponseEntity<Optional<Coments>>(HttpStatus.OK);
        } catch (NoSuchElementException exception) {
            return new ResponseEntity<Optional<Coments>>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation("Update coments, one at a time")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Coments> update(@PathVariable Long id, @RequestBody ComentsDTO dto) {
        Coments coments = comentsRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Registro não encontrado: " + id));

        persist(dto, coments);

        return ResponseEntity.ok().body(coments);
    }

    private void persist(@RequestBody ComentsDTO dto, Coments coments) {
        Waterfall waterfall = waterfallRepository.findById(dto.getWaterfallId())
                .orElseThrow(() -> new ObjectNotFoundException("Cachoeira não encontrada para esse id: " + dto.getWaterfallId()));

        coments.setWaterfall(waterfall);
        coments.setText(dto.getText());

        comentsRepository.save(coments);
    }
}
