package com.soft.ApiCachuApp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ComentsDTO {

    private Long waterfallId;
    private String text;

}
