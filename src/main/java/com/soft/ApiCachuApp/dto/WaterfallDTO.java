package com.soft.ApiCachuApp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class WaterfallDTO {

    private String link;
    private String image;
    private String name;
    private String longitude;
    private String latitude;
    private String localization;
    private String datetime;
    private String description;
}
