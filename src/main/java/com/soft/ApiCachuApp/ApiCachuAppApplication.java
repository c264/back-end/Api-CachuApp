package com.soft.ApiCachuApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCachuAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCachuAppApplication.class, args);
	}

}
