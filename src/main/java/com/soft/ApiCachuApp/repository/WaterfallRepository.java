package com.soft.ApiCachuApp.repository;

import com.soft.ApiCachuApp.model.Waterfall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WaterfallRepository extends JpaRepository<Waterfall, Long> {
}
