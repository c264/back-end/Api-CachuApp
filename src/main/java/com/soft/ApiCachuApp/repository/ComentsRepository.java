package com.soft.ApiCachuApp.repository;

import com.soft.ApiCachuApp.model.Coments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComentsRepository extends JpaRepository<Coments, Long> {
    List<Coments> findByWaterfallId(Long id);
}
